<?php

/**
 * Cat theme template file
 */

// if the right column region is populated, add a class called 'right-callout'



function Cat_preprocess_html(&$variables){
	if(!empty($variables['page']['right-callout'])){
		$variables['classes_array'][] = "right-callout";
	}
}

		

function cat_preprocess_page(&$variables){
	
	$widthClasses = array();
	$widthClasses['main'] = "";
	$widthClasses['column'] = "";
	
	//dpm($variables);
	// if both columns are populated
	if(!empty($variables['page']['left-callout']) and !empty($variables['page']['right-callout']))
		{$widthClasses['main'] = "one-half";
		$widthClasses['column'] = "one-fourth";
		}
	// if either column is populated
	else if(!empty($variables['page']['left-callout']) or !empty($variables['page']['right-callout']))
		{$widthClasses['main'] = "three-fourths";
		$widthClasses['column'] = "one-fourth";
		}
	// if none populated
	else{
		$widthClasses['main'] = "";
		}
	$variables['widthClasses'] = $widthClasses;
	//dpm($variables);
}	
	
	
?>