    <div id="site-header">
          <div class="container">
            <div id="logo">
                <img src="<?php print path_to_theme(); ?>/imgs/logo.png" title='Home' alt="Photography Blog" />
            </div>
        </div>     
	   <div id="site-header-image">
            <a href="/">
                <img src="<?php print path_to_theme(); ?>/imgs/grad_bar.png" title="Home" alt="Ivory Trunks" style='repeat:repeat-x;' />
            </a>
        </div>
    </div>
    <div class="container">
        <div class="menu-nav-container main">
            <?php	// print main menu
			print theme('links',array('links'=>$main_menu));
			?>
			<!--
			<ul>
                <li class="first active"><a href="#">Item 1</a></li>
                <li><a href="#">Item 2</a></li>
                <li><a href="#">Item 3</a></li>
                <li><a href="#">Item 4</a></li>
            </ul> -->
        </div>
    </div>
    <div class="container main-content">
        <div class="clearfix">
            <h1><?php print $title; ?></h1>
				<?php if ($messages): ?>
				<div id="messages"><div class="section clearfix">
				<?php print $messages; ?>
				</div></div> <!-- /.section, /#messages -->
				<?php endif; ?>
				
				<?php if ($tabs): ?>
				<div class="tabs">
				<?php print render($tabs); ?>
				</div>				
				<?php endif; ?>


			<?php //dpm($variables);?>	
	
			<!-- CREATE LEFT COLUMN -->
			
			<?php   if($page['left-callout']) : ?>
            <div class="<?php print $variables['widthClasses']['column'];?>">
				<?php print render($page['left-callout']); ?>
            </div>
			<?php endif; ?>	
			
			<!-- CREATE CONTENT -->
			
			 <div class="big-image <?php print $variables['widthClasses']['main'];?>">
				<?php print render($page['main-content']); ?>
            </div>
	
			
			<!-- CREATE RIGHT COLUMN -->
			
			<?php   if($page['right-callout']) : ?>
            <div class="<?php print $variables['widthClasses']['column'];?>">
				<?php print render($page['right-callout']); ?>
            </div>
			<?php endif; ?>		
			
			
        </div>
    </div>
    <div id="site-footer">
        <div class="container">
            <p>IPhone selvage et aesthetic aliquip. Lomo eiusmod laboris Bushwick Echo Park pug. Nesciunt iPhone gentrify, dreamcatcher officia delectus Tonx Schlitz bicycle rights nihil Odd Future. Pop-up craft beer sunt, Shoreditch tempor meh Brooklyn fixie asymmetrical sartorial mixtape. IPhone mixtape assumenda synth ad church-key, fanny pack sapiente pour-over squid salvia selfies jean shorts Odd Future sint. Pickled meh sustainable, roof party sriracha meggings vero trust fund McSweeney's eiusmod ethnic craft beer tempor butcher. Kitsch Brooklyn Pitchfork before they sold out, non viral cardigan Godard swag anim consequat voluptate.</p>
        </div>
    </div>